class SimpleClass {
    // объявление свойства
    public String var_ru = "значение по умолчанию";
    public String var_en = "default value";

    // объявление метода
    public String displayVarRu() {
        return var_ru;
    }

    // объявление метода
    public String displayVarEn() {
        return var_en;
    }

    // объявление метода
    public int displaySum(int val1, int val2) {
        return val1 + val2;
    }
}
