import javax.swing.*;        

public class Lab2SwingClass {
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    private static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("Lab2");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	SimpleClass sc = new SimpleClass();
        //Add the ubiquitous "Hello World" label.
        JLabel label1 = new JLabel(sc.displayVarRu());
        frame.getContentPane().add(label1);
        JLabel label2 = new JLabel(sc.displayVarEn());
        frame.getContentPane().add(label2);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}
