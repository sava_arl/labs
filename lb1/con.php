#!/usr/bin/php
<?php
require_once('SimpleClass.php');	
$me = new SimpleClass( ) ;
if ($argv[1] == "-h") {
	echo "Lab1: version 0.5 
Args:
	-h	Show this information.
	-s val1 val2	Show two messages and val1 + val2
Exaples:
	/usr/bin/php con.php -h
	/usr/bin/php con.php -s 1 2
";
} elseif ($argv[1] == "-s") {
	echo $me->displayVarRu( )."\n";
	echo $me->displayVarEn( )."\n";
	echo $me->displaySum($argv[2], $argv[3])."\n";
}
?>
