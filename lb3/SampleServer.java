import java.io.*;
import java.net.*;
class SampleServer extends Thread {
    Socket s;
    int num;
    public static void main(String args[]) {
        try {
            ServerSocket server = new ServerSocket(1500, 0, InetAddress.getByName("172.31.240.4"));
            System.out.println("server is started");
            while(true)
                new SampleServer(server.accept());
        } catch(Exception e) {System.out.println("init error: "+e);}
    }
    public SampleServer(Socket s) {
        //this.num = num;
        this.s = s;
        setDaemon(true);
        setPriority(NORM_PRIORITY);
        start();
    }
    public void run() {
        try {
            // из сокета клиента берём поток входящих данных
            //InputStream is = s.getInputStream();
            // и оттуда же - поток данных от сервера к клиенту
            //OutputStream os = s.getOutputStream();
            // буффер данных в 64 килобайта
            //byte buf[] = new byte[64*1024];
            // читаем 64кб от клиента, результат - кол-во реально принятых данных
            //int r = is.read(buf);
            // создаём строку, содержащую полученную от клиента информацию
            //String data = new String(buf, 0, r);
            // добавляем данные об адресе сокета:
            //data = ""+num+": "+"\n"+data;
            // выводим данные:
            //os.write(data.getBytes());
            // завершаем соединение
            //s.close();

		InputStream is = s.getInputStream();
		OutputStream os = s.getOutputStream();
		byte buf[] = new byte[64*1024];
		int r = is.read(buf);
		String data = new String(buf, 0, r);
		String delims = "[ ]+";
		String[] tokens = data.split(delims);
		String out_data = "HTTP/1.0 200 OK\r\nContent-Type: text/html\r\nConnection: close\r\n\r\n";
		String file_name;
		if (tokens[1].equals("/")) {
			file_name = "./index.html";
		} else {
			file_name = "." + tokens[1];
		}
		BufferedReader br;// = new BufferedReader(new FileReader(file_name));
		try {
		    br = new BufferedReader(new FileReader(file_name));
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();

		    while (line != null) {
			sb.append(line);
			sb.append(System.lineSeparator());
			line = br.readLine();
		    }
		    String everything = out_data + sb.toString();
		    os.write(everything.getBytes());
		    br.close();
		} catch(Exception e) {
		    String everything = "HTTP/1.0 404 OK\r\nContent-Type: t        ext/html\r\nConnection: close\r\n\r\n";
		    os.write(everything.getBytes());
		}
		s.close();
        } catch(Exception e) {System.out.println("init error: "+e);}
    }
}
