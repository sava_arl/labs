<?php
require_once('SimpleClass.php');	
$me = new SimpleClass( ) ;
if ($argv[1] == "-h") {
	echo "Lab1: version 0.5 
Args:
	-h	Show this information.
	-s val1 val2	Show two messages and val1 + val2
Exaples:
	/usr/bin/php con.php -h
	/usr/bin/php con.php -s 1 2
";
} elseif ($argv[1] == "-s") {
	echo $me->displayVarRu( )."\n";
	echo $me->displayVarEn( )."\n";
	echo $me->displaySum($argv[2], $argv[3])."\n";
}
else{
?>
<html>
 <head>
  <title>Тестируем PHP</title>
 </head>
 <body>
  <form method="post" action="./lab.php">
   <input type="text" name="txtName1" size="10" maxlength="5" value=<?php print $_POST['txtName1'];?>>
   <input type="text" name="txtName2" size="10" maxlength="5" value=<?php print $_POST['txtName2'];?>>
   <input type="submit" name="Старт" value="Старт">
  </form>
<?php
print $me->displayVarRu( )."<br>" ;
print $me->displayVarEn( )."<br>" ;
print $me->displaySum($_POST['txtName1'], $_POST['txtName2']) ;
?>
 </body>
</html>
<?php 
} 
