<?php
class SimpleClass
{
    // объявление свойства
    public $var_ru = 'значение по умолчанию';
    public $var_en = 'default value';

    // объявление метода
    public function displayVarRu() {
        return $this->var_ru;
    }

    // объявление метода
    public function displayVarEn() {
        return $this->var_en;
    }

    // объявление метода
    public function displaySum($val1, $val2) {
        return ($val1 + $val2);
    }
}
?>
