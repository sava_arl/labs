<?php
Header("Expires: 0");
Header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
Header("Cache-Control: no-cache, must-revalidate");
Header("Cache-Control: post-check=0,pre-check=0", false);
Header("Cache-Control: max-age=0", false);
Header("Pragma: no-cache");

$url_name = "http://172.31.240.4/soap/server.php";
$server = new SoapServer(null, array('uri' => $url_name,
											'encoding' => "utf-8"));
include_once("func.php");

$server->addFunction("my_func");
$server->handle();
?>
