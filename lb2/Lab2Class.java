class Lab2Class {

    public static void main(String[] args) {
        // Prints "Hello, World" to the terminal window.
	SimpleClass sc = new SimpleClass();
	if (args[0].equals("-s")) {
        	System.out.println(sc.displayVarRu());
        	System.out.println(sc.displayVarEn());
		try {
        		System.out.println(sc.displaySum(Integer.parseInt(args[1]), Integer.parseInt(args[2])));
		} catch (Throwable t){
			System.out.println("Wrong arguments!");
		}
	} else if (args[0].equals("-h")) {
        	System.out.println("Lab1: version 0.5\nArgs:\n\t-h\tShow this information.\n\t-s\tval1 val2\tShow two messages and val1 + val2\nExaples:\n\tjava Lab2Class -h\n\tjava Lab2Class -s 1 2");
	}
    }

}
